<?php
	$form_location = base_url().'your_account/submit';
?>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="<?= base_url().'assets/css/gridtest.css' ?>">
	</head>
	<body>
		<div class="Form">
				
				<?php
					echo validation_errors("<div class='alert alert-danger' role='alert'>", "</div><br /><br />");
				
					if (isset($flash)) 
					{
						echo $flash;
					} 
				?>
				
				<div class="formBox">	
				
					<img src="<?= base_url().'assets/img/spitfire2.jpg' ?>" height="120" width="300" />
					
					<h4>Register Your Account</h4>
					<br />
					
					<?php

							echo form_open($form_location);
							
							echo "<div class='inputLabel'><label>Username: &nbsp;&nbsp;&nbsp;</label></div><div class='inputBox'>";
							echo form_input('username', 'Username', 'class="form-control"');
							echo "</div>";
							
							echo "<div class='formSpacer'>&nbsp;</div>";
							
							echo "<div class='inputLabel'><label>E-Mail: &nbsp;&nbsp;&nbsp;</label></div><div class='inputBox'>";
							echo form_input('email', 'E-Mail', 'class="form-control"');
							echo "</div>";

							echo "<div class='formSpacer'>&nbsp;</div>";

							echo "<div class='inputLabel'><label>Password: &nbsp;&nbsp;&nbsp;</label></div><div class='inputBox'>";
							echo form_password('pword', '', 'class="form-control"');
							echo "</div>";
							
							echo "<div class='formSpacer'>&nbsp;</div>";
							
							echo "<div class='inputLabel'><label>Confirm Password: &nbsp;&nbsp;&nbsp;</label></div><div class='inputBox'>";
							echo form_password('confirm_pword', '', 'class="form-control"');
							echo "</div>";

							echo "<div class='formSpacer'>&nbsp;</div>";
						?>
					  <br />
				
				<?php

					echo '<div class="inputSubmit"><button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button></div>';

					echo form_close();
				
				?>
			</div>
		
			<?php

				echo validation_errors("<p style='color:red;'>","</p>");

			?>
		</div>
	</body>
</html>