<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Custom_pagination extends MX_Controller
{

function __construct()
{
parent::__construct();
}

function _draw_pagination($data)
{
	//$data variable needs $template, $target_base_url, $total_rows, $offset_segment, $limit

	$template = $data['template'];
	$target_base_url = $data['target_base_url'];
	$total_rows = $data['total_rows'];
	$offset_segment = $data['offset_segment'];
	$limit = $data['limit'];

	if ($template == "txrr_template")
	{
		$settings = $this->get_settings_for_txrr_template();
	}
	elseif ($template == 'cm_panel')
	{
		$settings = $this->get_settings_for_cmpanel_template();
	}

	$this->load->library('pagination');
	$config['base_url'] = $target_base_url;
	$config['total_rows'] = $total_rows;
	$config['uri_segment'] = $offset_segment;

	$config['per_page'] = $limit;
	$config['num_links'] = $settings['num_links'];

	$config['full_tag_open'] = $settings['full_tag_open'];
	$config['full_tag_close'] = $settings['full_tag_close'];

	$config['cur_tag_open'] = $settings['cur_tag_open'];
	$config['cur_tag_close'] = $settings['cur_tag_close'];

	$config['num_tag_open'] = $settings['num_tag_open'];
	$config['num_tag_close'] = $settings['num_tag_close'];

	$config['first_link'] = $settings['first_link'];
	$config['first_tag_open'] = $settings['first_tag_open'];
	$config['first_tag_close'] = $settings['first_tag_close'];

	$config['last_link'] = $settings['last_link'];
	$config['last_tag_open'] = $settings['last_tag_open'];
	$config['last_tag_close'] = $settings['last_tag_close'];

	$config['prev_link'] = $settings['prev_link'];
	$config['prev_tag_open'] = $settings['prev_tag_open'];
	$config['prev_tag_close'] = $settings['prev_tag_close'];

	$config['next_link'] = $settings['next_link'];
	$config['next_tag_open'] = $settings['next_tag_open'];
	$config['next_tag_close'] = $settings['next_tag_close'];

	$this->pagination->initialize($config);
	$pagination = $this->pagination->create_links();
	return $pagination;
}

function get_showing_statement($data)
{
	//Data variable needs to contain limit, offset, and total_rows

	$limit = $data['limit'];
	$offset = $data['offset'];
	$total_rows = $data['total_rows'];

	$value1 = $offset+1;
	$value2 = $offset+$limit;
	$value3 = $total_rows;

	if ($value2>$value3)
	{
		$value2 = $value3;
	}

	$showing_statement = '<span style="color:#999;">Showing '.$value1.' - '.$value2.' of '.$value3.' articles.</span>';
	return $showing_statement;

}

function get_settings_for_txrr_template()
{
	$settings['num_links'] = 5;

	$settings['full_tag_open'] = '<ul class="crap">';
	$settings['full_tag_close'] = '</ul>';

	$settings['cur_tag_open'] = '<div class="crapItem">';
	$settings['cur_tag_close'] = '</div>';

	$settings['num_tag_open'] = '<div class="crapItem">';
	$settings['num_tag_close'] = '</div>';

	$settings['first_link'] = 'First';
	$settings['first_tag_open'] = '<div class="crapItem">';
	$settings['first_tag_close'] = '</div>';

	$settings['last_link'] = 'Last';
	$settings['last_tag_open'] = '<div class="crapItem">';
	$settings['last_tag_close'] = '</div>';

	$settings['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
	$settings['prev_tag_open'] = '<div class="crapItem">';
	$settings['prev_tag_close'] = '</div>';

	$settings['next_link'] = '<span aria-hidden="true">&raquo;</span>';
	$settings['next_tag_open'] = '<div class="crapItem">';
	$settings['next_tag_close'] = '</div>';

	return $settings;

	/*
	<nav aria-label="Page navigation">
		<ul class="pagination justify-content-center">
		<li class="page-item"><a class="page-link" href="#">Previous</a></li>
		<li class="page-item"><a class="page-link" href="#">1</a></li>
		<li class="page-item"><a class="page-link" href="#">2</a></li>
		<li class="page-item"><a class="page-link" href="#">3</a></li>
		<li class="page-item"><a class="page-link" href="#">Next</a></li>
		</ul>
	</nav>
	*/
}

function get_settings_for_cmpanel_template()
{
	$settings['num_links'] = 10;

	$settings['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination">';
	$settings['full_tag_close'] = '</ul></nav>';

	$settings['cur_tag_open'] = '<li class="active"><a href="#">';
	$settings['cur_tag_close'] = '</a></li>';

	$settings['num_tag_open'] = '<li >';
	$settings['num_tag_close'] = '</li>';

	$settings['first_link'] = 'First';
	$settings['first_tag_open'] = '<li>';
	$settings['first_tag_close'] = '</li>';

	$settings['last_link'] = 'Last';
	$settings['last_tag_open'] = '<li>';
	$settings['last_tag_close'] = '</li>';

	$settings['prev_link'] = 'Prev';
	$settings['prev_tag_open'] = '<li>';
	$settings['prev_tag_close'] = '</li>';

	$settings['next_link'] = 'Next';
	$settings['next_tag_open'] = '<li>';
	$settings['next_tag_close'] = '</li>';
	return $settings;
}

/*
function get_settings_for_other_template()
{
	$settings['num_links'] = 2;

	$settings['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination">';
	$settings['full_tag_close'] = '</ul></nav>';

	$settings['cur_tag_open'] = '<li class="active"><a href="#">';
	$settings['cur_tag_close'] = '</a></li>';

	$settings['num_tag_open'] = '<li >';
	$settings['num_tag_close'] = '</li>';

	$settings['first_link'] = 'First';
	$settings['first_tag_open'] = '<li>';
	$settings['first_tag_close'] = '</li>';

	$settings['last_link'] = 'Last';
	$settings['last_tag_open'] = '<li>';
	$settings['last_tag_close'] = '</li>';

	$settings['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
	$settings['prev_tag_open'] = '<li>';
	$settings['prev_tag_close'] = '</li>';

	$settings['next_link'] = '<span aria-hidden="true">&raquo;</span>';
	$settings['next_tag_open'] = '<li>';
	$settings['next_tag_close'] = '</li>';
	return $settings;
}
*/

}
