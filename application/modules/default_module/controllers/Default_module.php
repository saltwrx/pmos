<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Default_module extends MX_Controller
{

function __construct()
{
parent::__construct();
}

function index()
{

	$first_bit = trim($this->uri->segment(1));

	if ($first_bit == '') {
		$first_bit = 'home';
	}

	$this->load->module('webpages');
	$query = $this->webpages->get_where_custom('page_url', $first_bit);
	$num_rows = $query->num_rows();

	if ($num_rows > 0)
	{
		//page was found, go ahead and load.
		foreach($query->result() as $row)
		{
			$data['headline'] = $row->page_title;
			$data['page_title'] = $row->page_title;
			$data['keywords'] = $row->keywords;
			$data['description'] = $row->description;
			$data['page_content'] = $row->page_content;
			$data['page_url'] = $row->page_url;
		}
	}
	else
	{
		//page not found
		$this->load->module('site_settings');
		$data['page_content'] = $this->site_settings->_get_page_not_found_msg();
	}

	$this->load->module('templates');
	$this->templates->txrr_template($data);
}

}
